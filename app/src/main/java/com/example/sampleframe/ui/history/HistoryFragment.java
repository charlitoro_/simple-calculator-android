package com.example.sampleframe.ui.history;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import com.example.sampleframe.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class HistoryFragment extends Fragment {
    @BindView(R.id.tvHistory)
    TextView tvHistory;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_history, container, false);
        ButterKnife.bind(this, view);
        SharedPreferences preferences = this.getActivity().getSharedPreferences("data", Context.MODE_PRIVATE);
        this.tvHistory.setText(preferences.getString("history", ""));
        return view;
    }

    @OnClick(R.id.btnCleanHistory)
    public void cleanHistoryClicked() {
        SharedPreferences preferences = this.getActivity().getSharedPreferences("data", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.clear();
        editor.apply();
        this.tvHistory.setText(R.string.emptyText);
    }
}