package com.example.sampleframe.ui.calculab;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import com.example.sampleframe.R;

import org.mariuszgromada.math.mxparser.Expression;
import org.w3c.dom.Text;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnTextChanged;
import butterknife.Optional;

public class CalculabFragment extends Fragment {
    @BindView(R.id.tvResult)
    TextView tvResult;
    @BindView(R.id.tvLastOpe)
    TextView tvLastOpe;

    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_calculate, container, false);
        ButterKnife.bind(this, view);
        // TODO: Use fields...
        return view;
    }

    @Optional
    @OnClick({R.id.btn0, R.id.btn1, R.id.btn2, R.id.btn3, R.id.btn4,
            R.id.btn5, R.id.btn6, R.id.btn7, R.id.btn8, R.id.btn9,
            R.id.btnPlus, R.id.btnLess, R.id.btnMul, R.id.btnDat,
            R.id.btnDiv, R.id.btnExp, R.id.btnBracketLef, R.id.btnBracketRig})
    public void numberClicked(Button btn) {
        String result = (String)this.tvResult.getText();
        result = result + btn.getText();
        this.tvResult.setText(result);
    }

    @OnClick(R.id.btnEqual)
    public void equalClicked() {
        String strExpression = (String)this.tvResult.getText();
        Expression resExpression = new Expression(strExpression);
        String result = String.valueOf(resExpression.calculate());
        this.tvResult.setText(result);
        this.tvLastOpe.setText(result);
        // Save Operation
        SharedPreferences preferences = this.getActivity().getSharedPreferences("data", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        String strHistory = preferences.getString("history", "");
        strHistory = strHistory.concat(getString(R.string.historyExpLabel) + strExpression + "\n" + getString(R.string.historyResLabel) + result + "\n\n");
        editor.putString("history", strHistory);
        editor.apply();
    }

    @Optional
    @OnClick(R.id.btnClean)
    public void cleanClicked() {
        this.tvResult.setText(R.string.emptyText);
        this.tvLastOpe.setText(R.string.emptyText);
    }

    @Optional
    @OnClick({R.id.btnSin, R.id.btnCos, R.id.btnTan,
            R.id.btnLn})
    public void functionClicked(Button btn) {
        String result = (String)this.tvResult.getText();
        result = (String)btn.getText() + '(' + result + ')';
        this.tvResult.setText(result);
    }

    @OnClick(R.id.btnRoll)
    public void rollbackClicked() {
        String strExpression = (String)this.tvResult.getText();
        if (strExpression.length() > 0)
            this.tvResult.setText(strExpression.substring(0, strExpression.length()-1));
    }

    @Optional
    @OnClick( R.id.btnFac)
    public void factorialClicked() {
        String result = (String)this.tvResult.getText();
        result = result + "!";
        this.tvResult.setText(result);
    }
}
