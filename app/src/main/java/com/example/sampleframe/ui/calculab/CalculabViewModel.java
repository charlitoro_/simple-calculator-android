package com.example.sampleframe.ui.calculab;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

public class CalculabViewModel extends ViewModel {

    private MutableLiveData<String> mText;

    public CalculabViewModel() {
        mText = new MutableLiveData<>();
        mText.setValue("This is calculab fragment");
    }

    public LiveData<String> getText() {
        return mText;
    }
}